(function ($) {

  Drupal.behaviors.passwordGenerate = {
    attach: function () {
      $( ".password-copy" ).click(function() {
        tc = document.createElement('textarea');
        tc.value = $(this).data('password');
        document.body.appendChild(tc);
        tc.select();
        document.execCommand('copy');
        document.body.removeChild(tc);
      });
    }
  };


})(jQuery);
