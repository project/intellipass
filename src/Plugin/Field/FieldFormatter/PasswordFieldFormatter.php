<?php

namespace Drupal\intellipass\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\intellipass\Service\EncryptionService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "password_field_formatter",
 *   label = @Translation("Password formatter"),
 *   field_types = {
 *     "text",
 *     "string",
 *   }
 * )
 */
class PasswordFieldFormatter extends FormatterBase {

  /**
   * @var \Drupal\intellipass\Service\EncryptionService
   */
  private $encryptionService;

  /**
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  private $twigEnvironment;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EncryptionService $encryptionService, TwigEnvironment $twigEnvironment) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->encryptionService = $encryptionService;
    $this->twigEnvironment = $twigEnvironment;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('intellipass.encryption'), $container->get('twig'));
  }


  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Provides a copy function for the password.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => $this->render($this->encryptionService->decrypt($item->value))];
    }

    return $element;
  }

  /**
   * Render.
   */
  public function render(string $password) {
    $path = drupal_get_path('module', 'intellipass') . '/' . 'templates/password.html.twig';
    $twigTemplate = $this->twigEnvironment->load($path);

    return $twigTemplate->render(['password' => $password]);
  }

}
