<?php

namespace Drupal\intellipass\Listener;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class KernelEventListener.
 *
 * @package Drupal\permissions_by_term
 */
class RedirectListener implements EventSubscriberInterface
{
  /**
   * @var CurrentPathStack
   */
  private $currentPathStack;

  /**
   * @var RequestStack
   */
  private $requestStack;

  /**
   * @var AccountProxy
   */
  private $account;

  public function __construct(
    CurrentPathStack $currentPathStack,
    RequestStack $requestStack,
    AccountProxy $account
  )
  {
    $this->currentPathStack = $currentPathStack;
    $this->requestStack = $requestStack;
    $this->account = $account;
  }

  /**
   * Access restriction on kernel request.
   */
  public function onKernelRequest(GetResponseEvent $event)
  {
    if ($this->account->id() !== 0) {
      $userPageWithId = Url::fromRoute('user.page')->toString() . '/' . $this->account->id();
      if ($this->requestStack->getCurrentRequest()->headers->get('referer') === null && $this->currentPathStack->getPath() == $userPageWithId) {
        return $this->redirectToContent();
      }
    }
  }

  private function redirectToContent()
  {
    $redirect = new RedirectResponse(Url::fromRoute('system.admin_content')->toString());
    return $redirect->send();
  }

  /**
   * The subscribed events.
   */
  public static function getSubscribedEvents()
  {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
    ];
  }

}
